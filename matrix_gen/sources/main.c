#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int ParseArgs(int argc, char* argv[]);
int* CreateMatrix(int row_count, int column_count);
int WriteMatrix(char* filename, int* data, int row_count, int column_count);

int mat_count;
int m_rows;
int n_columns;

int main(int argc, char* argv[]){
	mat_count	= 1;
	m_rows		= 1;
	n_columns	= 1;

	int* matrix;
	char* filename = calloc(50, sizeof(char));
	int result = 0;

	if (ParseArgs(argc, argv) == 0)
		return 1;

	srand(time(NULL));

	printf("Creating matrix with %d lines and %d columns...\n", m_rows, n_columns);

	for (int i = 0; i < mat_count && result >= 0; ++i){
		matrix = CreateMatrix(m_rows, n_columns);

		printf("Matrix created.\n");

		if (sprintf(filename, "matrix_%d.txt", i + 1) <= 0){
			result = -1;
		}
		else{
			printf("Writing matrix to file '%s'...\n", filename);
			result = WriteMatrix(filename, matrix, m_rows, n_columns);
		}

		printf("Matrix write operation return: %d\n", result);

		free(matrix);
	}

	printf("Freeing allocated memory...\n");
	free(filename);

	return result;
}

int ParseArgs(int argc, char* argv[]){
	if (argc < 3){
		printf("Usage: %s #rows #columns [#matrices]\n", argv[0]);

		return 0;
	}

	m_rows		= atoi(argv[1]);
	n_columns	= atoi(argv[2]);

	if (argc > 3)
		mat_count = atoi(argv[3]);

	return 1;
}

int* CreateMatrix(int nlines, int ncolumns){
	int nelements = nlines * ncolumns;
	int* buffer;
	buffer = calloc(nelements, sizeof(int));

	for (int i = 0; i < nelements; i++){
		buffer[i] = rand() % 5;
	}

	return buffer;
}

int WriteMatrix(char* filename, int* data, int nlines, int ncolumns){
	FILE* file = fopen(filename, "w");
	char* buffer = calloc(5, sizeof(char));
	int status_ok = 1;

	if (file == NULL)
		return -1;

	fprintf(file, "LINHAS = %d\n", nlines);
	fprintf(file, "COLUNAS = %d\n", ncolumns);

	for (int i = 0; i < nlines && status_ok == 1; i++){
		for (int j = 0; j < ncolumns && status_ok == 1; j++){
			if (sprintf(buffer, "%d", data[i * ncolumns + j]) < 0)
				status_ok = 0;
			else{
				if (j < ncolumns - 1)
					fprintf(file, "%s ", buffer);
				else
					fprintf(file, "%s", buffer);
			}
		}

		if (i < nlines - 1)
			fprintf(file, "\n");
	}

	fclose(file);
	return status_ok;
}
