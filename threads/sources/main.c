#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "matrix.h"
#include "matmult.h"

typedef struct{
	matrix m1, m2, result;
	int start, count;
} matmultsegment_t;

void ParseArgs(int argc, char* argv[]);
void PrintAllMatrices(matrix *m1, matrix *m2, matrix *m3);

void* ThreadMultiply(void* args);

int thread_count;

int main(int argc, char* argv[]){
	matrix m1, m2, result;
	thread_count = 0;

	ParseArgs(argc, argv);
	if (thread_count == 0)
		exit(1);

	ReadMatrixFromFile("in1.txt", &m1);
	ReadMatrixFromFile("in2.txt", &m2);

	AllocateMatrix(&result, m1.rows, m2.columns);
	AttachMatrixSharedData(&result);

	int remain = m1.rows % thread_count;
	int rows_per_thread = (int)(m1.rows / thread_count);
	int start = 0, row_count;

	pthread_t tid[thread_count];
	matmultsegment_t args[thread_count];

	for (int thread_i = 0; thread_i < thread_count; thread_i++){
		row_count = rows_per_thread;

		if (remain > 0){
			row_count++;
			remain--;
		}

		args[thread_i].start	= start;
		args[thread_i].count	= row_count;
		args[thread_i].m1		= m1;
		args[thread_i].m2		= m2;
		args[thread_i].result	= result;

		pthread_create(&tid[thread_i], NULL,
			ThreadMultiply, (void*)&args[thread_i]);

		start += row_count;
	}

	for (int thread_i = 0; thread_i < thread_count; thread_i++)
		pthread_join(tid[thread_i], NULL);

	WriteMatrixToFile("out.txt", &result);

	//PrintAllMatrices(&m1, &m2, &result);

	FreeMatrix(&m1);
	FreeMatrix(&m2);
	FreeMatrix(&result);

	return 0;
}

void ParseArgs(int argc, char* argv[]){
	if (argc == 1){
		printf("Usage: %s #processes\n", argv[0]);
		return;
	}

	thread_count = atoi(argv[1]);
}

void* ThreadMultiply(void* args){
	matmultsegment_t *params = (matmultsegment_t*)args;
	AttachMatrixSharedData(&(params->m1));
	AttachMatrixSharedData(&(params->m2));
	AttachMatrixSharedData(&(params->result));

	MultiplyMatrix_Segment(&(params->m1), &(params->m2),
		params->start, params->count, &(params->result));

	DetachMatrixSharedData(&(params->m1));
	DetachMatrixSharedData(&(params->m2));
	DetachMatrixSharedData(&(params->result));

	pthread_exit(0);
}

void PrintAllMatrices(matrix *m1, matrix *m2, matrix *m3){
	printf("\tMatrix 1:\n");
	PrintMatrix(m1);

	printf("\n\tMatrix 2:\n");
	PrintMatrix(m2);

	printf("\n\tMultiplication result:\n");
	PrintMatrix(m3);
}
