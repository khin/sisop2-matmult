#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <string.h>
#include "matrix.h"

void AllocateMatrix(matrix *m, int rows, int columns){
	if (m == NULL) return;

	int size = rows * columns * sizeof(int);
	m->rows			= rows;
	m->columns		= columns;
	m->segment_id	= shmget(IPC_PRIVATE, size, S_IRUSR | S_IWUSR);
}

void AttachMatrixSharedData(matrix *m){
	if (m == NULL) return;
	m->elements = (int*) shmat(m->segment_id, NULL, 0);
}

void DetachMatrixSharedData(matrix *m){
	if (m == NULL) return;

	if (m->elements != NULL){
		shmdt(m->elements);
		m->elements	= NULL;
	}
}

void FreeMatrix(matrix *m){
	if (m == NULL) return;

	if (m->elements != NULL){
		shmdt(m->elements);
		shmctl(m->segment_id, IPC_RMID, NULL);
		m->elements	= NULL;
	}

	m->rows		= 0;
	m->columns	= 0;
}

int GetMatrixElement(matrix *m, int i, int j){
	if (m == NULL) return 0;

	if (i > m->rows || j > m->columns || i < 0 || j < 0){
		printf("\tTried to access element (%d, %d) of a %dx%d matrix\n",
													i, j, m->rows, m->columns);
		return 0;
	}

	return m->elements[i * m->columns + j];
}

int SetMatrixElement(matrix *m, int i, int j, int value){
	if (m == NULL) return 0;

	if (i > m->rows || j > m->columns || i < 0 || j < 0){
		printf("\tTried to access element (%d, %d) of a %dx%d matrix\n",
													i, j, m->rows, m->columns);
		return 0;
	}

	m->elements[i * m->columns + j] = value;
	return 1;
}

int ReadMatrixFromFile(const char* filename, matrix *m){
	if (m == NULL) return 0;

	FILE *file = fopen(filename, "r");

	if (file == NULL){
		printf("\tCannot open file '%s'\n", filename);
		return 0;
	}

	int rows, columns, element, status_ok = 1;
	char line_buffer[8192], *element_buffer;

	fgets(line_buffer, 20, file); sscanf(line_buffer, "LINHAS = %d", &rows);
	fgets(line_buffer, 20, file); sscanf(line_buffer, "COLUNAS = %d", &columns);

	AllocateMatrix(m, rows, columns);
	AttachMatrixSharedData(m);

	for (int i = 0; i < rows && status_ok; i++){
		fgets(line_buffer, 8192, file);

		element_buffer = strtok(line_buffer, " ");
		element = atoi(element_buffer);
		SetMatrixElement(m, i, 0, element);

		for (int j = 1; j < columns && status_ok; j++){
			element_buffer = strtok(NULL, " ");

			if (element_buffer == NULL){
				printf("\tTried to access row out of bound: %d\n", j);
				status_ok = 0;
			}
			else{
				element = atoi(element_buffer);
				SetMatrixElement(m, i, j, element);
			}
		}

		if (feof(file) && i < rows - 1)
			status_ok = 0;
	}

	fclose(file);
	return status_ok;
}

int WriteMatrixToFile(const char* filename, matrix *m){
	if (m == NULL) return 0;

	int status_ok = 1;
	char *buffer = calloc(5, sizeof(char));

	FILE *file = fopen(filename, "w");
	if (file == NULL)
		return 0;

	fprintf(file, "LINHAS = %d\n", m->rows);
	fprintf(file, "COLUNAS = %d\n", m->columns);

	for (int i = 0; i < m->rows && status_ok; i++){
		for (int j = 0; j < m->columns && status_ok; j++){
			if (sprintf(buffer, "%d", GetMatrixElement(m, i, j)) < 0)
				status_ok = 0;
			else{
				if (j < m->columns - 1)
					fprintf(file, "%s ", buffer);
				else
					fprintf(file, "%s", buffer);
			}
		}

		if (i < m->rows - 1)
			fprintf(file, "\n");
	}

	fclose(file);
	return status_ok;
}

void PrintMatrix(matrix *m){
	if (m == NULL) return;

	for (int i = 0; i < m->rows; i++){
		for (int j = 0; j < m->columns; j++)
			printf("%d ", GetMatrixElement(m, i, j));

		printf("\n");
	}
}
