#include <stdlib.h>
#include <stdio.h>
#include "matmult.h"

int MultiplyMatrix_Element(matrix *m1, matrix *m2, int i, int j){
	if (m1 == NULL || m2 == NULL || i < 0 || j < 0 ||
		m1->rows != m2->columns || i >= m1->rows || j >= m2->columns){
			printf("\tError multiplying matrix element\n");
			return -1;
		}

	int result	= 0;
	int max		= m1->rows;

	for (int index = 0; index < max; index++){
		result +=
			GetMatrixElement(m1, i, index) * GetMatrixElement(m2, index, j);
	}

	return result;
}

void MultiplyMatrix_Row(matrix *m1, matrix *m2, int i, matrix *result){
	if (m1 == NULL || m2 == NULL || result == NULL || i < 0 || i > m1->rows){
		printf("\tError multiplying matrix row\n");
		return;
	}

	int max		= m2->columns;
	int value	= 0;

	for (int j = 0; j < max; j++){
		value = MultiplyMatrix_Element(m1, m2, i, j);
		SetMatrixElement(result, i, j, value);
	}
}

void MultiplyMatrix_Segment(matrix *m1, matrix *m2, int start, int count, matrix *result){
	int max = start + count;

	for (int index = start; index < max; index++){
		MultiplyMatrix_Row(m1, m2, index, result);
	}
}
