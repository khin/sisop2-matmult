#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "matrix.h"
#include "matmult.h"

void ParseArgs(int argc, char* argv[]);
void PrintAllMatrices(matrix *m1, matrix *m2, matrix *m3);

void ProcessMultiply(matrix m1, matrix m2, int start, int count, matrix m3);

int process_count;

int main(int argc, char* argv[]){
	matrix m1, m2, result;
	process_count = 0;

	ParseArgs(argc, argv);
	if (process_count == 0)
		exit(1);

	ReadMatrixFromFile("in1.txt", &m1);
	ReadMatrixFromFile("in2.txt", &m2);

	AllocateMatrix(&result, m1.rows, m2.columns);
	AttachMatrixSharedData(&result);

	int remain = m1.rows % process_count;
	int rows_per_process = (int)(m1.rows / process_count);
	int start = 0;
	pid_t pid[process_count];

	for (int process_i = 0; process_i < process_count; process_i++){
		pid[process_i] = fork();

		if (pid[process_i] == 0){

			if (remain > 0){
				rows_per_process += 1;
			}

			ProcessMultiply(m1, m2, start, rows_per_process, result);
		}
		else{
			if (remain > 0){
				start += rows_per_process + 1;
				remain--;
			}
			else
				start += rows_per_process;
		}
	}

	for (int process_i = 0; process_i < process_count; process_i++)
		waitpid(pid[process_i], 0, 0);

	WriteMatrixToFile("out.txt", &result);

	//PrintAllMatrices(&m1, &m2, &result);

	FreeMatrix(&m1);
	FreeMatrix(&m2);
	FreeMatrix(&result);

	return 0;
}

void ParseArgs(int argc, char* argv[]){
	if (argc == 1){
		printf("Usage: %s #processes\n", argv[0]);
		return;
	}

	process_count = atoi(argv[1]);
}

void ProcessMultiply(matrix m1, matrix m2, int start, int count, matrix m3){
	AttachMatrixSharedData(&m1);
	AttachMatrixSharedData(&m2);
	AttachMatrixSharedData(&m3);

	MultiplyMatrix_Segment(&m1, &m2, start, count, &m3);

	DetachMatrixSharedData(&m1);
	DetachMatrixSharedData(&m2);
	DetachMatrixSharedData(&m3);

	exit(0);
}

void PrintAllMatrices(matrix *m1, matrix *m2, matrix *m3){
	printf("\tMatrix 1:\n");
	PrintMatrix(m1);

	printf("\n\tMatrix 2:\n");
	PrintMatrix(m2);

	printf("\n\tMultiplication result:\n");
	PrintMatrix(m3);
}
