#pragma once

#include <stdlib.h>

typedef struct {
	int rows;
	int columns;
	int segment_id;
	int* elements;
} matrix;

void AllocateMatrix(matrix *m, int rows, int columns);
void AttachMatrixSharedData(matrix *m);
void DetachMatrixSharedData(matrix *m);
int GetMatrixElement(matrix *m, int i, int j);
int SetMatrixElement(matrix *m, int i, int j, int value);
void FreeMatrix(matrix *m);

int ReadMatrixFromFile(const char* filename, matrix *m);
int WriteMatrixToFile(const char* filename, matrix *m);
void PrintMatrix(matrix *m);
