#pragma once

#include <stdlib.h>
#include "matrix.h"

/*
	Multiply row 'i' in matrix 'm1' and column 'j' in matrix 'm2'
	Results the single value of the result
*/
int MultiplyMatrix_Element(matrix *m1, matrix *m2, int i, int j);

/*
	Multiply row 'i' of matrix 'm1' with every column of matrix 'm2'.
	Resulting row is stored in matrix 'result'
*/
void MultiplyMatrix_Row(matrix *m1, matrix *m2, int i, matrix *result);

/*
	Multiply 'count' rows of 'm1' (starting from row 'start') with
	'count' columns of 'm2', storing the result in the appropriate
	rows in 'result'
*/
void MultiplyMatrix_Segment(matrix *m1, matrix *m2,
	int start, int count, matrix *result);
